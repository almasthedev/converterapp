import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  constructor() { 
    this.counter=1;
  }

  ngOnInit() {
  }

  counter: number;

  increase() {
    this.counter++;
  }
  decrease() {
    this.counter--;
  }


}
